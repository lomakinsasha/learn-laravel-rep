<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('author')->default('Ломакин Александр');
            $table->text('body');
            $table->boolean('status')->default(1);
            $table->string('photo_link');
            $table->string('author_photo_link');
            $table->integer('likes')->default(0);
            $table->integer('id_logo')->default(1);
            $table->integer('id_filter')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
