<?php

namespace App\Http\Controllers;

use App\Download;
use Illuminate\Http\Request;
use App;
use Illuminate\Support\Facades\Auth;

// Получить текущего аутентифицированного пользователя...

class ImageController extends Controller
{

    public function upload(Request $request) {

        $path = $request->file()[0]->store('uploads', 'public');

        $request->session()->put('variable', $path);

    }

    public function uploadText(Request $request) {

        $user = Auth::user();

        $text = $request->input('body');

        $path = $request->session()->get('variable');

        $post = App\Post::create([
            'photo_link' => asset('/storage/' . $path),
            'author_photo_link' => asset('/storage/' . $path),
            'body' => $text,
            'author' => $user->name,
        ]);


        return redirect('/');
    }

}
