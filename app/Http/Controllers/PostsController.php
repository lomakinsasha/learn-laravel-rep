<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App;

class PostsController extends Controller
{
    public function index() {
        if (Auth::check()) {
            $auth = true;
        } else {
            $auth = false;
        }
        $stories = App\Post::orderBy('created_at', 'desc')->get();

        return view('posts.index')->with('stories', $stories)->with('auth', $auth);
    }

    public function show() {
        return view('posts.show');
    }

    public function store(Request $request) {
        dump($request->all());
    }
}
