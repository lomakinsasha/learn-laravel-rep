<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PostsController@index');
Route::get('/posts/{post}', 'PostsController@show');
Route::post('/upload', 'ImageController@upload')->name('image.upload');
Route::post('/', 'ImageController@uploadText')->name('image.upload');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::post('/login', ['uses'=>'Auth\LoginController@login']);
Route::post('/register', ['uses'=>'Auth\RegisterController@register']);