<div class="m-auto">
    <h1 class="main__title">АЛЛЕЯ СЛАВЫ<br>
        БЕГОВЫХ КРОССОВОК ASICS</h1>
</div>
<div class="stories-panel">
    <div class="stories-filter">
        <ul class="stories-filter__list">
            <li class="stories-filter__list-item"><a class="stories-filter__link active" href="#">Популяные</a></li>
            <li class="stories-filter__list-item"><a class="stories-filter__link" href="#">Обсуждаемые</a></li>
            <li class="stories-filter__list-item"><a class="stories-filter__link" href="#">Победители</a></li>
            <li class="stories-filter__list-item"><a class="stories-filter__link" href="#">Истории 2018</a></li>
        </ul>
    </div>
    <div class="stories-panel__search-container">
        <form action="" class="stories-panel__search">
            <label class="story-search__label" for>
                <input class="story-search__input" type="text" placeholder="Поиск">
            </label>
        </form>
        @if($auth)
            <button class="stories-panel__btn main-btn ">добавить историю</button>
        @else
            <button class="main-btn showLoginPopup">добавить историю</button>
        @endif
    </div>
</div>