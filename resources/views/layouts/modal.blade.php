<div class="load-modal load-modal--step-1 load-modal--open" style="display: none" data-post="">
    <a class="load-modal__close-btn load-modal__close-btn--snickers"></a>
    <input id="id-post" value="" type="hidden" name="">
    <div class="load-modal__content"><div class="load-modal__content-wrapper">

            <div class="load-modal__header">
                <p class="load-modal__title">
                    Шаг 1
                </p>
                <p class="load-modal__description">
                    Загрузите фотографию своих кроссовок
                </p>
            </div>
            <div class="load-modal__dragndrop-form-container">
                <form enctype="multipart/form-data" action="{{ route('image.upload') }}" method="POST" class="load-modal__dragndrop-form">
                    <div class="load-modal__dragndrop-area" id="dropZone">
                        <img class="load-modal__photo-img" src="https://runandwin.ru/alleyaslavy/images/photo-icon.svg" alt="">
                        <p class="load-modal__dragndrop-area-desc load-modal__dragndrop-area-desc--blue">
                            К участию принимаются только <br> кроссовки бренда ASICS
                        </p>
                        <input type="file" name="photo" id="photo-input"  accept="image/*" hidden="">
                    </div>
                    @csrf
                </form>
            </div>
            <div class="load-modal__filter-container load-modal__filter-container--bottom disabled">
                <div class="load-modal__btn-container">
                    <button class="load-modal-edit__btn main-btn" style="display: none;">Редактировать</button>
                    <button class="load-modal__btn alt-btn not-active">
                        Далее
                    </button>

                </div>

            </div>
        </div>
    </div>

    <div class="load-modal__progress-bar-container">
        <p class="load-modal__progress-bar-percents">40%</p>
        <div class="load-modal__progress-bar-full">
            <div class="load-modal__progress-bar-indicator" style="width: 40%;"></div>
        </div>
    </div>
</div>
