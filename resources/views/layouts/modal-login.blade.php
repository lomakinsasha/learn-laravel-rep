<div class="login-modal modal" style="display: none" id="loginModal">
    <a class="load-modal__close-btn modal-close-btn"></a>

    <div class="login-modal__content">
        <div class="load-modal__header">
            <p class="login-modal__title load-modal__title">
                Войти через
            </p>
        </div>

        <div class="login-modal__social-container">
            <a href="https://runandwin.ru/alleyaslavy/auth/vkontakte" class="login-modal__social-btn">

            </a>
            <a href="https://runandwin.ru/alleyaslavy/auth/facebook" class="login-modal__social-btn">

            </a>
            <a href="#" class="login-modal__social-btn login-modal__social-btn--asics runandwinLogin">
            </a>
        </div>


        <p class="login-modal__social-decsription">
            или использовать E-mail для входа
        </p>

        <div class="login-modal__allerts allert-message"></div>

        <form action="{{ route('login') }}" class="login-modal__form">
            @csrf
            <span class="invalid-feedback">
                    <strong>Не правильный логин или пароль</strong>
            </span>
            <label class="login-modal__label login-modal__label--email" for="">
                <input id="login-email" type="email" class="login-modal__input login-modal__input--email @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Email" autofocus>

                @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </label>
            <label class="login-modal__label login-modal__label--pass" for="">
                <input id="login-password" type="password" class="login-modal__input login-modal__input--pass @error('password') is-invalid @enderror" name="password" placeholder="Пароль" required autocomplete="current-password">

                @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </label>
            <button type="submit" class="login-modal__submit-btn main-btn">Войти</button>
        </form>

        <p class="login-modal__register-offer">Нет аккаунта? <a id="showReg" onclick="ym(54015763, 'reachGoal', 'register_click');">Зарегистрироваться</a></p>
        <p class="login-modal__register-offer" style="margin-top:0.8vw;"><small><a href="{{ route('password.request') }}" class="resetPass">Забыли пароль?</a></small></p>
    </div>
</div>