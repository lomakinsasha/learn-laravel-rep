<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Title</title>
    <link rel="stylesheet" href="{{ URL::asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/main.css') }}">
</head>
<body>

<div class="wrapper">
    <div class="container main-container">
        @include('layouts.nav')
        @yield('content')
    </div>
    @include('layouts.modal')
    @include('layouts.modal-login')
    @include('layouts.modal-register')
</div>
@include('layouts.footer')
</body>
</html>