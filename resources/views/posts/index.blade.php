@extends('layout')

@section('content')
    <div class="stories stories--home">
    @foreach($stories as $story)
            <div class="story-card story-card--id-{{ $story->id }}" data-id = "{{ $story->id }}">
                <div class="story-card__photo" style="background-image: url({{ $story->photo_link }})">
                    <img class="sticker sticker--card" src="https://runandwin.ru/alleyaslavy/images/stickers/sticker-1.svg" style="left: 63.272%; top: 3.223%">
                    <a class="story-card__author">
                        <div class="story-card__author-photo" style="background-image: url({{ $story->author_photo_link }})">

                        </div>
                        <p class="story-card__author-name">
                            {{ $story->author }}
                        </p>
                    </a>
                    <div class="story-card__metrics">
                        <div class="story-card__like-container">
                            <a class="story-card__icon like-icon"></a>
                            <span class="story-card__like-count">{{ $story->likes }}</span>
                        </div>
                        <div class="story-card__comments-container">
                            <a class="story-card__icon comment-icon"></a>
                            <span class="story-card__comment-count">0</span>
                        </div>
                    </div>
                </div>
                <div class="story-card__text">
                    {{ $story->body }}
                </div>
            </div>

        @endforeach
    </div>

@endsection

